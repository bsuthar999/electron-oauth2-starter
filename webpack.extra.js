const webpack = require('webpack');

module.exports = {
  'target': 'electron-renderer',
  'module': {
    'rules': [
      { test: /\.node$/, loader: 'node-loader' },
    ]
  }
};
