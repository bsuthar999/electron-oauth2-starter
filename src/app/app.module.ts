import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedImportsModule } from './shared-imports/shared-imports.module';
import { StorageService } from './common/services/storage-service/storage.service';
import { HomeComponent } from './home/home.component';
import { OAuth2Service } from './common/services/oauth2/oauth2.service';

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedImportsModule,
  ],
  providers: [AppService, StorageService, OAuth2Service],
  bootstrap: [AppComponent],
})
export class AppModule {}
